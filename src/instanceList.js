import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import axios from 'axios';
const drawerWidth = 240;
var url = "http://52.189.223.179:8080/kie-server/services/rest/server/containers/nswtg_1.3.0-SNAPSHOT/processes/instances";
var testURL = "https://reqres.in/api/users?page=2";



class InstanceList extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        data:{}
      };
    }


    componentDidMount() {
      axios.get(url, {
        auth: {
          username:"rhpamAdmin",
          password: "rhpamAdmin!23"
        }, headers: {
          "Content-Type": "application/json",
          "accept": "application/json"
        }
      })
      .then(res => {
        const result = res.data;
        // console.log(result["process-instance"]);
        JSON.parse(JSON.stringify(result["process-instance"]));
        this.setState({ data:result});
      })



      
    }


    createExpandablePanel() {
      let instanceArray = [];
      let temp = JSON.parse(JSON.stringify(this.state.data))["process-instance"];
      if(temp != null && temp != undefined) {
      console.log("temp is: " + temp["process-instance"]);
      for(var i=0; i < temp.length; i++) {

        instanceArray.push(
        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label="Expand"
            aria-controls="additional-actions1-content"
            id="additional-actions1-header"
          >
            <FormControlLabel
              aria-label="Acknowledge"
              onClick={event => event.stopPropagation()}
              onFocus={event => event.stopPropagation()}
      control={ <div><span >Process Instance ID: {temp[i]["process-instance-id"]}</span>
  <Divider orientation="vertical" flexItem />
      <span>Process Name: {temp[i]["process-name"]}</span>
              <Divider orientation="vertical" flexItem />
      <span>Process Version: {temp[i]["process-version"]}</span><Divider orientation="vertical" flexItem />
      <span>Initiator: {temp[i]["initiator"]}</span><Divider orientation="vertical" flexItem />
              </div>
            }
              // label="I acknowledge that I should stop the click event propagation"
            />
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography color="textSecondary">
              <pre>{JSON.stringify(this.state.data, null, 2) }</pre>
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        )
      }

      return instanceArray;
    }
    }

    render() {
      if(this.state.data != null) {
      var instanceArray = this.state.data["process-instance"];
      } else {
        instanceArray = "Empty";
      }



      return (

        <div>
    {this.createExpandablePanel()}

      </div>
    );
    }
  }
  
  export default InstanceList;