import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Radio from '@material-ui/core/Radio';
import Grid from '@material-ui/core/Grid';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import axios from 'axios';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Alert from '@material-ui/lab/Alert';
import CloseIcon from '@material-ui/icons/Close';
import Collapse from '@material-ui/core/Collapse';


import config from './config.json'
const drawerWidth = 240;
var url = config.pamURL;
var username = config.pamUsername;
var password = config.pamPassword;
var pamContainerID = config.pamContainerID


class TaskList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: {},
      duplicate: "false",
      taskInputs: [],
      taskStatus: [],
      alertBar: false,
      currentUser: "Admin"
    };
  }


  componentDidMount() {
    var allTasksURL = url + "/queries/tasks/instances/pot-owners";
    axios.get(allTasksURL, {
      auth: {
        username: username,
        password: password
      }, headers: {
        "Content-Type": "application/json",
        "accept": "application/json"
      }
    })
      .then(res => {
        var taskSummary = JSON.parse(JSON.stringify(res.data["task-summary"]));
     
        var result = [];

        
        // console.log(result["process-instance"]);
        // result = JSON.parse(JSON.stringify(result["task-summary"]));
        // console.log(result);
        
          for (var i = 0; i < taskSummary.length; i++) {
              // console.log(result[i]["task-status"]);
              if(taskSummary[i]["task-container-id"] == "somdispatcher_1.0.86") {
                result.push(taskSummary[i]);
                this.pamGetTaskInputs(result[i]["task-id"]);    
              }
          }
        
        this.setState({ data: result });
        
      })
  }


  updateTaskList() {
    var allTasksURL = url + "/queries/tasks/instances/admins";
    axios.get(allTasksURL, {
      auth: {
        username: username,
        password: password
      }, headers: {
        "Content-Type": "application/json",
        "accept": "application/json"
      }
    })
      .then(res => {
        var taskSummary = JSON.parse(JSON.stringify(res.data["task-summary"]));
     
        var result = [];
        
        // console.log(result["process-instance"]);
        // result = JSON.parse(JSON.stringify(result["task-summary"]));
        // console.log(result);
        
          for (var i = 0; i < taskSummary.length; i++) {
              // console.log(result[i]["task-status"]);
              if(taskSummary[i]["task-container-id"] == "somdispatcher_1.0.86") {
                result.push(taskSummary[i]);
                this.pamGetTaskInputs(result[i]["task-id"]);    
              }
          }
        
        this.setState({ data: result });
        

      })
  }
  pamGetTaskInputs = (taskID) => {
    var getInputURL = url + "/containers/" + pamContainerID + "/tasks/" + taskID + "/contents/input";
    // console.log(getInputURL);
    axios.get(getInputURL, {
      auth: {
        username: username,
        password: password
      }, headers: {
        "Content-Type": "application/json",
        "accept": "application/json"
      },
      data: JSON.stringify({ duplicate: this.state.duplicate }),
    })
      .then(res => {
        var result = this.state.taskInputs;
        // console.log("success");
        var tmpJSON = {};
        tmpJSON["taskID" + taskID + "inputs"] = res.data;
        result.push(tmpJSON);

        // console.log("Task Inputs: " + JSON.parse(JSON.stringify(result)));
        this.setState({ taskInputs: result });
        // console.log("input is:" + JSON.stringify(result));
        return result;
      })
  }


  pamStartTask = taskID => event => {
    var getInputURL = url + "/containers/" + pamContainerID + "/tasks/" + taskID + "/states/started";

    event.preventDefault();

    var taskData = {
      taskID: taskID,
      taskStatus: true
    }

    // console.log(taskData);
    this.setState({ taskStatus: taskData });

    axios(getInputURL, {
      method: 'PUT',
      auth: {
        username: username,
        password: password
      },
      headers: {
        "Content-Type": "application/json",
        "accept": "application/json"
      },
      data: {},

    })
      .then(res => {
        console.log("Started Task: " + taskID);
        this.updateTaskList();
        this.setState({ alertBar: true });
      })
      .catch(err => {
        console.log("Failed Start Task, Error is: " + err);
      })

  }



  pamCompleteTask = taskID => event => {
    var getInputURL = url + "/containers/" + pamContainerID + "/tasks/" + taskID + "/states/completed";
    
    event.preventDefault();

    var myDuplicateFound;

    if(this.state.duplicate == "false") {
      myDuplicateFound = false;
    } else {
      myDuplicateFound = true;
    }
    // console.log(getInputURL);
    axios.put(getInputURL, {}, {
      auth: {
        username: username,
        password: password
      }, headers: {
        "Content-Type": "application/json",
        "accept": "application/json"
      },
      data: {
        duplicateFound: myDuplicateFound
      }
    })
      .then(res => {
        console.log("Completed Task: " + taskID);
        this.updateTaskList();
        this.setState({ alertBar: true });
      })
      .catch(err => {
        console.log("Failed Complete Task, Error is: " + err);
      })

  }

  pamReleaseTask = taskID => event => {
    var getInputURL = url + "/containers/" + pamContainerID + "/tasks/" + taskID + "/states/released";

    event.preventDefault();

    var taskData = {
      taskID: taskID,
      taskStatus: true
    }

    // console.log(taskData);
    this.setState({ taskStatus: taskData });

    axios(getInputURL, {
      method: 'PUT',
      auth: {
        username: username,
        password: password
      },
      headers: {
        "Content-Type": "application/json",
        "accept": "application/json"
      },
      // data: JSON.stringify({duplicate: this.state.duplicate}),

    })
      .then(res => {
        console.log("Released Task: " + taskID);
        this.updateTaskList();
        this.setState({ alertBar: true });
      })
      .catch(err => {
        console.log("Failed Release Task, Error is: " + err);
      })

  }

  handleRadioChange = (event) => {
    this.setState({ duplicate: event.target.value });
    // console.log(this.state.duplicate);
  };

  handleUserChange = (event) => {
    this.setState({ currentUser: event.target.value });
  };


  // onSubmitDuplicate(taskID) {
  //   event.preventDefault();
  //   console.log("send rest!");
  // }

  //   closeAlertBar () {


  //     this.timer = setTimeout(function(){
  //       this.setState({ alertBar: false });
  //  }.bind(this),3000);

  //   }

  searchTaskInput(taskId) {
    var allInputs = this.state.taskInputs;
    var foundVar = null;
    var returnVar = {};
    // console.log("allinputs" + JSON.stringify(allInputs));
    for (var i = 0; i < allInputs.length; i++) {
      var key = Object.keys(allInputs[i]);
      var value = allInputs[i];

      if (key == "taskID" + taskId + "inputs") {
        foundVar = allInputs[i][key];
        break;
      }
    }
    // console.log(foundVar);
    if (foundVar != null || foundVar != undefined) {
      // console.log("Found the var!");
      returnVar.errorMessage = foundVar.errorMessage;
      returnVar.errorCode = foundVar.errorCode;
    }
    return returnVar;
  }

  searchTaskStatus(taskId) {
    var allInputs = this.state.taskStatus;
    var foundVar = null;
    for (var i = 0; i < allInputs.length; i++) {
      var key = Object.keys(allInputs[i]);
      var value = allInputs[i];

      if (key == taskId) {
        foundVar = allInputs[i][key];
        break;
      }
    }
    return foundVar;
  }


  createExpandablePanel() {
    let taskArray = [];
    // let temp = JSON.parse(JSON.stringify(this.state.data))["process-instance"];
    var data = this.state.data;
    if (data != null && data != undefined) {
      // console.log("temp is: " + temp["process-instance"]);
      for (var i = 0; i < data.length; i++) {

        taskArray.push(
          <ExpansionPanel key={"Task" + data[i]["task-id"]}>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-label="Expand"
              aria-controls="additional-actions1-content"
              id="additional-actions1-header"
            >
              <FormControlLabel
                aria-label="Acknowledge"
                onClick={event => event.stopPropagation()}
                onFocus={event => event.stopPropagation()}
                control={<div><span >Task ID: {data[i]["task-id"]}</span>
                  <Divider orientation="vertical" flexItem />
                  <span>Task Name: {data[i]["task-name"]}</span>
                  <Divider orientation="vertical" flexItem />
                  <span>Task Status: {data[i]["task-status"]}</span><Divider orientation="vertical" flexItem />
                  <span>Owner: {data[i]["task-actual-owner"]}</span><Divider orientation="vertical" flexItem />
                </div>
                }
              // label="I acknowledge that I should stop the click event propagation"
              />
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              {this.taskForm(data[i]["task-id"], data[i]["task-status"], data[i]["task-name"])}
            </ExpansionPanelDetails>
          </ExpansionPanel>

        )
      }

      return taskArray;
    }
  }





  taskForm(TaskId, TaskStatus, TaskName) {

    return (
      <div>
        <FormControl component="fieldset">
          <FormLabel component="legend">Task ID: {TaskId}</FormLabel>
          <br />

          {/* {(() => {
            var foundObject = (this.searchTaskInput(TaskId));
            console.log("foundObj is : " + typeof foundObject);
          })()} */}

          <span> Error Code : {(this.searchTaskInput(TaskId))["errorCode"]}</span>

          <span> Error Message : {(this.searchTaskInput(TaskId)).errorMessage}</span>
          {/* <Input defaultValue={this.pamGetTaskInputs(TaskId).errorMessage} disabled inputProps={{ 'aria-label': 'description' }} /> */}
          {/* <Input defaultValue="Disabled" disabled inputProps={{ 'aria-label': 'description' }} /> */}

          {(() => {

            if (TaskName == "Manual  Idempotency  Check") {

           return(<div><RadioGroup aria-label="gender" name="gender1" value={this.state.duplicate} onChange={this.handleRadioChange}>
            <FormControlLabel value="true" control={<Radio />} label="Duplicate" />
            <FormControlLabel value="false" control={<Radio />} label="Not Duplicate" />
          </RadioGroup>
          <br /></div>)

            }
          })()}


        {(() => {
            if (TaskStatus == "Ready") {
              return (<Button type="submit" variant="contained" color="primary" disabled={this.searchTaskStatus(TaskId)} onClick={this.pamStartTask(TaskId)}>
                Claim
              </Button>)
            } else if(TaskStatus == "Completed") {} else {
              return (
          <Grid container spacing={3}>

        <Grid item xs={6}>
        <Button type="submit" variant="contained" color="primary" onClick={this.pamCompleteTask(TaskId)}>
                       Submit
           </Button>
        </Grid>
        <Grid item xs={6}>
        <Button type="submit" variant="contained" color="primary" onClick={this.pamReleaseTask(TaskId)}>
                       Release
             </Button>
        </Grid>
      </Grid>
              )
            }
          })()}


        </FormControl>
      </div>
    );
  }

  render() {
    if (this.state.data != null) {
      var taskArray = this.state.data["process-instance"];
    } else {
      taskArray = "Empty";
    }



    return (

      <div>
        <Collapse in={this.state.alertBar}>
          {/* {this.closeAlertBar()} */}
          <Alert
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  this.setState({ alertBar: false });
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            Request Successful.
        </Alert>
        </Collapse>


          {/* Current User: */}
        {/* <Select
          labelId="select-user"
          id="select-user"
          value={this.state.currentUser}
          onChange={this.handleUserChange}
        >
          <MenuItem value={"Admin"}>Admin</MenuItem>
          <MenuItem value={"BobBrown"}>Bob Brown</MenuItem>
          <MenuItem value={"JohnSmith"}>John Smith</MenuItem>
        </Select> */}
        {/* <br /><br /> */}
        {this.createExpandablePanel()}

      </div>
    );
  }
}

export default TaskList;